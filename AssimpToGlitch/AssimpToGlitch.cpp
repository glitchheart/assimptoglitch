// AssimpToGlitch.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <assimp\Importer.hpp>
#include <assimp/scene.h> 
#include <assimp/material.h>
#include <assimp/postprocess.h>

using i32 = int; 
using b32 = i32;
using u32 = unsigned int;

struct vec3
{
	float X;
	float Y;
	float Z;
};


struct color
{
	float R;
	float G;
	float B;
	float A;
};

struct quat
{
	float X;
	float Y;
	float Z;
	float W;
};

struct model_header
{
	char Format[4];
	char Version[4];
};

struct mesh_data
{
	i32 BaseVertex;
	i32 BaseIndex;
	i32 MaterialIndex;
	i32 NumIndices;
};

struct texture_info
{
	b32 HasData;
	char TextureName[50];
	i32 TextureHandle;
};

struct material
{
	texture_info DiffuseTexture;
	color Color;
};

struct model_data
{
	i32 Type;

	i32 NumMeshes;
	i32 NumVertices;
	i32 NumIndices;
	i32 NumBones;
	i32 NumMaterials;

	b32 HasNormals;
	b32 HasUVs;

	long MeshChunkSize;
	long VertexBufferChunkSize;
	long IndexBufferChunkSize;
	long MaterialChunkSize;
	long BoneChunkSize;

	float GlobalInverseMatrix[4][4];
};

struct vertex_bone_info
{
	int Count;
	std::vector<u32> Ids;
	std::vector<float> Weights;

	std::vector<u32> FinalIds;
	std::vector<float> FinalWeights;

	vertex_bone_info()
	{
		Count = 0;
	}
};

#define MAX_CHILDREN 30
#define MAX_BONES 50

struct bone
{
	char Name[30];
	i32 Parent = -1;
	u32 Children[MAX_CHILDREN];
	i32 ChildCount;
	
	float Transformation[4][4];
	float BoneOffset[4][4];
};

struct texture_data
{
	long TextureChunkSize;
};

struct animation_position_channel
{
	i32 NumKeys;
	std::vector<float> TimeStamps;
	std::vector<vec3> Values;
};

struct animation_rotation_channel
{
	i32 NumKeys;
	std::vector<float> TimeStamps;
	std::vector<quat> Values;
};

struct animation_scaling_channel
{
	i32 NumKeys;
	std::vector<float> TimeStamps;
	std::vector<vec3> Values;
};

struct bone_channels
{
	i32 BoneIndex;
	animation_position_channel PositionKeys;
	animation_rotation_channel RotationKeys;
	animation_scaling_channel ScalingKeys;
};

struct skeletal_animation
{
	char Name[30];
	float Duration;
	i32 NumBoneChannels;
	std::vector<bone_channels> BoneChannels;
};

struct animation_channel_header
{
	float Duration;
	i32 NumBoneChannels;
};

struct animation_header
{
	i32 NumAnimations;
};

struct bone_animation_header
{
	i32 BoneIndex;
	i32 NumPositionChannels;
	i32 NumRotationChannels;
	i32 NumScalingChannels;
};

struct bone_channel_header
{
	i32 NumKeys;
};

#define FOR(List, Count) for(int Index = 0; Index < Count; Index++)

static void LoadBones(u32 MeshIndex, const aiMesh* Mesh, const std::vector<mesh_data>& Entries, std::vector<bone>& BoneInfoList, std::vector<vertex_bone_info>& Bones, std::map<std::string, u32>& BoneMap, i32* NumBones)
{
	u32 BoneIndex = (u32)(*NumBones);

	for (u32 I = 0; I < Mesh->mNumBones; I++) 
	{
		std::string BoneName(Mesh->mBones[I]->mName.data);

		if (BoneMap.find(BoneName) == BoneMap.end())
		{
			// Allocate an index for a new bone
			BoneIndex = (u32)*NumBones;
			(*NumBones)++;

			bone bi;
			sprintf_s(bi.Name, "%s", Mesh->mBones[I]->mName.data);

			BoneInfoList.push_back(bi);

			BoneInfoList[BoneIndex].BoneOffset[0][0] = Mesh->mBones[I]->mOffsetMatrix[0][0];
			BoneInfoList[BoneIndex].BoneOffset[0][1] = Mesh->mBones[I]->mOffsetMatrix[0][1];
			BoneInfoList[BoneIndex].BoneOffset[0][2] = Mesh->mBones[I]->mOffsetMatrix[0][2];
			BoneInfoList[BoneIndex].BoneOffset[0][3] = Mesh->mBones[I]->mOffsetMatrix[0][3];
			BoneInfoList[BoneIndex].BoneOffset[1][0] = Mesh->mBones[I]->mOffsetMatrix[1][0];
			BoneInfoList[BoneIndex].BoneOffset[1][1] = Mesh->mBones[I]->mOffsetMatrix[1][1];
			BoneInfoList[BoneIndex].BoneOffset[1][2] = Mesh->mBones[I]->mOffsetMatrix[1][2];
			BoneInfoList[BoneIndex].BoneOffset[1][3] = Mesh->mBones[I]->mOffsetMatrix[1][3];
			BoneInfoList[BoneIndex].BoneOffset[2][0] = Mesh->mBones[I]->mOffsetMatrix[2][0];
			BoneInfoList[BoneIndex].BoneOffset[2][1] = Mesh->mBones[I]->mOffsetMatrix[2][1];
			BoneInfoList[BoneIndex].BoneOffset[2][2] = Mesh->mBones[I]->mOffsetMatrix[2][2];
			BoneInfoList[BoneIndex].BoneOffset[2][3] = Mesh->mBones[I]->mOffsetMatrix[2][3];
			BoneInfoList[BoneIndex].BoneOffset[3][0] = Mesh->mBones[I]->mOffsetMatrix[3][0];
			BoneInfoList[BoneIndex].BoneOffset[3][1] = Mesh->mBones[I]->mOffsetMatrix[3][1];
			BoneInfoList[BoneIndex].BoneOffset[3][2] = Mesh->mBones[I]->mOffsetMatrix[3][2];
			BoneInfoList[BoneIndex].BoneOffset[3][3] = Mesh->mBones[I]->mOffsetMatrix[3][3];

			BoneMap[BoneName] = BoneIndex;

			for (u32 j = 0; j < Mesh->mBones[I]->mNumWeights; j++)
			{
				u32 VertexID = (u32)Entries[MeshIndex].BaseVertex + Mesh->mBones[I]->mWeights[j].mVertexId;
				float Weight = Mesh->mBones[I]->mWeights[j].mWeight;

				Bones[VertexID].Count++;
				Bones[VertexID].Ids.push_back(I);
				Bones[VertexID].Weights.push_back(Weight);
			}
		}
		else
		{
			BoneIndex = BoneMap[BoneName];
		}
	}
}

struct node
{
	i32 Depth = 0;
	aiNode* Node;
};

static node FindRootBoneNode(aiNode* ParentNode, const std::map <std::string, u32>& BoneMap, i32 Depth)
{
	Depth++;
	node RootBoneNode;
	RootBoneNode.Node = 0;

	if (BoneMap.find(ParentNode->mName.data) != BoneMap.end())
	{
		RootBoneNode.Node = ParentNode;
		RootBoneNode.Depth = Depth;
	}
	else
	{
		i32 Highest = 1000;
		for (int ChildIndex = 0; ChildIndex<ParentNode->mNumChildren; ChildIndex++)
		{
			auto Found = FindRootBoneNode(ParentNode->mChildren[ChildIndex], BoneMap, Depth);
			if (Found.Node && Found.Depth < Highest)
			{
				Highest = Found.Depth;
				RootBoneNode = Found;
			}
		}
	}

	return RootBoneNode;
}

static aiNode* FindNextBone(aiNode* Node, const std::map<std::string, u32>& BoneMap)
{
	aiNode* FoundNode = 0;

	for (int ChildIndex = 0; ChildIndex<Node->mNumChildren; ChildIndex++)
	{
		FoundNode = FindRootBoneNode(Node->mChildren[ChildIndex], BoneMap, 0).Node;
		if (FoundNode)
			break;
	}

	return FoundNode;
}

static void BuildSkeleton(aiNode* RootBoneNode, i32 ParentId, std::map <std::string, u32>& BoneMap, std::vector<bone>& Bones)
{
	if (BoneMap.find(RootBoneNode->mName.data) == BoneMap.end())
	{
		RootBoneNode = FindNextBone(RootBoneNode, BoneMap);
	}

	u32 BoneIndex = BoneMap[RootBoneNode->mName.data];
	bone& Bone = Bones[BoneIndex];
	Bone.Parent = ParentId;
	Bone.ChildCount = RootBoneNode->mNumChildren;

	Bone.Transformation[0][0] = RootBoneNode->mTransformation[0][0];
	Bone.Transformation[0][1] = RootBoneNode->mTransformation[0][1];
	Bone.Transformation[0][2] = RootBoneNode->mTransformation[0][2];
	Bone.Transformation[0][3] = RootBoneNode->mTransformation[0][3];
	Bone.Transformation[1][0] = RootBoneNode->mTransformation[1][0];
	Bone.Transformation[1][1] = RootBoneNode->mTransformation[1][1];
	Bone.Transformation[1][2] = RootBoneNode->mTransformation[1][2];
	Bone.Transformation[1][3] = RootBoneNode->mTransformation[1][3];
	Bone.Transformation[2][0] = RootBoneNode->mTransformation[2][0];
	Bone.Transformation[2][1] = RootBoneNode->mTransformation[2][1];
	Bone.Transformation[2][2] = RootBoneNode->mTransformation[2][2];
	Bone.Transformation[2][3] = RootBoneNode->mTransformation[2][3];
	Bone.Transformation[3][0] = RootBoneNode->mTransformation[3][0];
	Bone.Transformation[3][1] = RootBoneNode->mTransformation[3][1];
	Bone.Transformation[3][2] = RootBoneNode->mTransformation[3][2];
	Bone.Transformation[3][3] = RootBoneNode->mTransformation[3][3];

	for (int ChildIndex = 0; ChildIndex < Bone.ChildCount; ChildIndex++)
	{
		u32 ChildBoneIndex = BoneMap[RootBoneNode->mChildren[ChildIndex]->mName.data];
		Bone.Children[ChildIndex] = ChildBoneIndex;

		BuildSkeleton(RootBoneNode->mChildren[ChildIndex], (i32)BoneIndex, BoneMap, Bones);
	}
}

void SortBoneWeights(std::vector<vertex_bone_info>& BoneData)
{
	for (int Index = 0; Index < BoneData.size(); Index++)
	{
		vertex_bone_info& Info = BoneData[Index];

		if (Info.Count <= 4)
		{
			for (int WeightIndex = 0; WeightIndex < Info.Weights.size(); WeightIndex++)
			{
				Info.FinalIds.push_back(Info.Ids[WeightIndex]);
				Info.FinalWeights.push_back(Info.Weights[WeightIndex]);
			}
		}
		else
		{
			int Count = 0;

			std::vector<i32> addedIndices;

			while (Count < 4)
			{
				float LastHighest = 0.0f;
				i32 LastIndex = -1;

				for (int WeightIndex = 0; WeightIndex < Info.Weights.size(); WeightIndex++)
				{
					bool AlreadyAdded = false;
					for (int AIndex = 0; AIndex < addedIndices.size(); AIndex++)
					{
						AlreadyAdded = addedIndices[AIndex] == WeightIndex;
						if (AlreadyAdded)
							break;
					}

					if (!AlreadyAdded && Info.Weights[WeightIndex] > LastHighest)
					{
						LastIndex = WeightIndex;
						LastHighest = Info.Weights[WeightIndex];
					}
				}
				
				if (LastIndex >= 0)
				{
					Info.FinalIds.push_back(Info.Ids[LastIndex]);
					Info.FinalWeights.push_back(Info.Weights[LastIndex]);
					addedIndices.push_back(LastIndex);
					Count++;
					LastIndex = -1;
					LastHighest = 0.0f;
				}
			}

			float Combined = Info.FinalWeights[0] + Info.FinalWeights[1] + Info.FinalWeights[2] + Info.FinalWeights[3];

			for (int WeightIndex = 0; WeightIndex < 4; WeightIndex++)
			{
				Info.FinalWeights[WeightIndex] = (1.0f / Combined) * Info.FinalWeights[WeightIndex];
			}

			Info.Count = 4;
		}
	}
}

int main()
{
	Assimp::Importer Importer;
	const aiScene* Scene = Importer.ReadFile("B:/Models/model.dae", aiProcess_Triangulate | aiProcess_GenSmoothNormals |
		aiProcess_FlipUVs);// | aiProcess_JoinIdenticalVertices);
	auto RootNode = Scene->mRootNode;
	
	model_data ModelData;
	ModelData.NumVertices = 0;
	ModelData.NumIndices = 0;
	ModelData.NumMaterials = 0;
	ModelData.HasNormals = Scene->mMeshes[0]->HasNormals();
	ModelData.HasUVs = Scene->mMeshes[0]->HasTextureCoords(0);

	std::vector<mesh_data> Entries;
	std::vector<material> Materials;

	std::vector<float*> Vertices;
	std::vector<float*> Normals;
	std::vector<float*> TextureCoords;
	std::vector<u32> Indices;
	std::vector<vertex_bone_info> BoneData;
	std::map<std::string, u32> BoneMap;
	std::vector<bone> Bones;

	i32 NumBones = 0;

	FOR(Scene->mMeshes, Scene->mNumMeshes)
	{
		Entries.push_back(mesh_data());
		auto Mesh = Scene->mMeshes[Index];

		Entries[Index].MaterialIndex = Scene->mMeshes[Index]->mMaterialIndex;
		Entries[Index].NumIndices = Scene->mMeshes[Index]->mNumFaces * 3;
		Entries[Index].BaseVertex = ModelData.NumVertices;
		Entries[Index].BaseIndex = ModelData.NumIndices;

		ModelData.NumVertices += Scene->mMeshes[Index]->mNumVertices;
		ModelData.NumIndices += Entries[Index].NumIndices;
	}

	// @Incomplete: Remember to check if we even have a texture here
	FOR(Scene->mMaterials, Scene->mNumMaterials)
	{
		material Mat;
		Mat.DiffuseTexture.TextureHandle = -1;
		Mat.DiffuseTexture.HasData = true;

		auto Material = Scene->mMaterials[Index];

		aiString TextureName;
		Material->GetTexture(aiTextureType_DIFFUSE, 0, &TextureName);
		std::string TexStr(TextureName.data);

		auto Text = TexStr.substr(0, TexStr.length() - 4);
		auto TxtPtr = Text.c_str();

		sprintf_s(Mat.DiffuseTexture.TextureName, "%s", TxtPtr);
		Materials.push_back(Mat);
	}

	FOR(Scene->mMeshes, Scene->mNumMeshes)
	{
		auto MeshEntry = Entries[Index];
		auto Mesh = Scene->mMeshes[Index];

		for (int VertexIndex = 0; VertexIndex < Mesh->mNumVertices; VertexIndex++)
		{
			float* Vertex = (float*)malloc(sizeof(float) * 3);
			float* Normal = (float*)malloc(sizeof(float) * 3);
			float* TexCoord = (float*)malloc(sizeof(float) * 2);

			Vertex[0] = Mesh->mVertices[VertexIndex].x;
			Vertex[1] = Mesh->mVertices[VertexIndex].y;
			Vertex[2] = Mesh->mVertices[VertexIndex].z;

			if (Mesh->mNormals)
			{
				Normal[0] = Mesh->mNormals[VertexIndex].x;
				Normal[1] = Mesh->mNormals[VertexIndex].y;
				Normal[2] = Mesh->mNormals[VertexIndex].z;
			}
			else
			{
				Normal[0] = 0.0f;
				Normal[1] = 0.0f;
				Normal[2] = 0.0f;
			}

			if (Mesh->mTextureCoords[0])
			{
				TexCoord[0] = Mesh->mTextureCoords[0][VertexIndex].x;
				TexCoord[1] = Mesh->mTextureCoords[0][VertexIndex].y;
			}
			else
			{
				TexCoord[0] = 0.0f;
				TexCoord[1] = 0.0f;
			}
			
			Vertices.push_back(Vertex);
			Normals.push_back(Normal);
			TextureCoords.push_back(TexCoord);
			BoneData.push_back(vertex_bone_info());
		}

		LoadBones(Index, Mesh, Entries, Bones, BoneData, BoneMap, &NumBones);
		SortBoneWeights(BoneData);

		for (u32 FaceIndex = 0; FaceIndex < Mesh->mNumFaces; FaceIndex++)
		{
			const aiFace& Face = Mesh->mFaces[FaceIndex];
			assert(Face.mNumIndices == 3);
			Indices.push_back(MeshEntry.BaseIndex + Face.mIndices[0]);
			Indices.push_back(MeshEntry.BaseIndex + Face.mIndices[1]);
			Indices.push_back(MeshEntry.BaseIndex + Face.mIndices[2]);
		}
	}

	// Build skeleton
	aiNode* RootBoneNode = FindRootBoneNode(Scene->mRootNode, BoneMap, 0).Node;

	if (RootBoneNode)
	{
		BuildSkeleton(RootBoneNode, -1, BoneMap, Bones);

		auto GlobalInverse = RootNode->mTransformation.Inverse();
		ModelData.GlobalInverseMatrix[0][0] = GlobalInverse[0][0];
		ModelData.GlobalInverseMatrix[0][1] = GlobalInverse[0][1];
		ModelData.GlobalInverseMatrix[0][2] = GlobalInverse[0][2];
		ModelData.GlobalInverseMatrix[0][3] = GlobalInverse[0][3];
		ModelData.GlobalInverseMatrix[1][0] = GlobalInverse[1][0];
		ModelData.GlobalInverseMatrix[1][1] = GlobalInverse[1][1];
		ModelData.GlobalInverseMatrix[1][2] = GlobalInverse[1][2];
		ModelData.GlobalInverseMatrix[1][3] = GlobalInverse[1][3];
		ModelData.GlobalInverseMatrix[2][0] = GlobalInverse[2][0];
		ModelData.GlobalInverseMatrix[2][1] = GlobalInverse[2][1];
		ModelData.GlobalInverseMatrix[2][2] = GlobalInverse[2][2];
		ModelData.GlobalInverseMatrix[2][3] = GlobalInverse[2][3];
		ModelData.GlobalInverseMatrix[3][0] = GlobalInverse[3][0];
		ModelData.GlobalInverseMatrix[3][1] = GlobalInverse[3][1];
		ModelData.GlobalInverseMatrix[3][2] = GlobalInverse[3][2];
		ModelData.GlobalInverseMatrix[3][3] = GlobalInverse[3][3];
	}
	else
	{
		// @Incomplete: No bones? We need a way to prompt an error message
	}
	
	ModelData.NumBones = NumBones;

	// Load animations
	std::vector<skeletal_animation> SkeletalAnimations;

	for (int AnimationIndex = 0; AnimationIndex < Scene->mNumAnimations; AnimationIndex++)
	{
		auto Animation = Scene->mAnimations[AnimationIndex];
		skeletal_animation SkeletalAnimation;
		sprintf_s(SkeletalAnimation.Name, "%s", Animation->mName.data);
		SkeletalAnimation.Duration = Animation->mDuration;

		for (int ChannelIndex = 0; ChannelIndex < Animation->mNumChannels; ChannelIndex++)
		{
			auto Channel = Animation->mChannels[ChannelIndex];
			if (BoneMap.find(Channel->mNodeName.data) != BoneMap.end())
			{
				u32 BoneIndex = BoneMap[Channel->mNodeName.data];

				bone_channels BoneChannels;
				BoneChannels.BoneIndex = BoneIndex;

				BoneChannels.PositionKeys.NumKeys = Channel->mNumPositionKeys;
				BoneChannels.RotationKeys.NumKeys = Channel->mNumRotationKeys;
				BoneChannels.ScalingKeys.NumKeys = Channel->mNumScalingKeys;

				for (int Index = 0; Index < Channel->mNumPositionKeys; Index++)
				{
					auto Key = Channel->mPositionKeys[Index];
					BoneChannels.PositionKeys.TimeStamps.push_back(Key.mTime);
					
					vec3 Vec;
					Vec.X = Key.mValue.x;
					Vec.Y = Key.mValue.y;
					Vec.Z = Key.mValue.z;

					BoneChannels.PositionKeys.Values.push_back(Vec);
				}

				for (int Index = 0; Index < Channel->mNumRotationKeys; Index++)
				{
					auto Key = Channel->mRotationKeys[Index];

					BoneChannels.RotationKeys.TimeStamps.push_back(Key.mTime);
					
					quat Quat;
					Quat.X = Key.mValue.x;
					Quat.Y = Key.mValue.y;
					Quat.Z = Key.mValue.z;
					Quat.W = Key.mValue.w;

					BoneChannels.RotationKeys.Values.push_back(Quat);
				}

				for (int Index = 0; Index < Channel->mNumScalingKeys; Index++)
				{
					auto Key = Channel->mScalingKeys[Index];

					BoneChannels.ScalingKeys.TimeStamps.push_back(Key.mTime);

					vec3 Vec;
					Vec.X = Key.mValue.x;
					Vec.Y = Key.mValue.y;
					Vec.Z = Key.mValue.z;
					BoneChannels.ScalingKeys.Values.push_back(Vec);
				}

				SkeletalAnimation.BoneChannels.push_back(BoneChannels);
			}
		}
		SkeletalAnimation.NumBoneChannels = SkeletalAnimation.BoneChannels.size();
		SkeletalAnimations.push_back(SkeletalAnimation);
	}

	texture_data TextureData;

	std::vector<float> VertexBuffer;

	int VertexSize = 3;
	int NormalSize = ModelData.HasNormals ? 3 : 0;
	int TexCoordSize = ModelData.HasUVs ? 2 : 0;;
	int BoneIndexSize = ModelData.NumBones > 0 ? 4 : 0;
	int BoneWeightSize = ModelData.NumBones > 0 ? 4 : 0;

	int TotalSize = VertexSize + NormalSize + TexCoordSize + BoneIndexSize + BoneWeightSize;

	for (i32 Index = 0; Index < ModelData.NumVertices; Index++)
	{
		VertexBuffer.push_back(Vertices[Index][0]);
		VertexBuffer.push_back(Vertices[Index][1]);
		VertexBuffer.push_back(Vertices[Index][2]);

		if (ModelData.HasNormals)
		{
			VertexBuffer.push_back(Normals[Index][0]);
			VertexBuffer.push_back(Normals[Index][1]);
			VertexBuffer.push_back(Normals[Index][2]);
		}

		if (ModelData.HasUVs)
		{
			VertexBuffer.push_back(TextureCoords[Index][0]);
			VertexBuffer.push_back(TextureCoords[Index][1]);
		}
		
		if (ModelData.NumBones > 0)
		{
			if (BoneData[Index].Count > 0)
				VertexBuffer.push_back(BoneData[Index].FinalIds[0]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 1)
				VertexBuffer.push_back(BoneData[Index].FinalIds[1]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 2)
				VertexBuffer.push_back(BoneData[Index].FinalIds[2]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 3)
				VertexBuffer.push_back(BoneData[Index].FinalIds[3]);
			else
				VertexBuffer.push_back(0);

			if (BoneData[Index].Count > 0)
				VertexBuffer.push_back(BoneData[Index].FinalWeights[0]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 1)
				VertexBuffer.push_back(BoneData[Index].FinalWeights[1]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 2)
				VertexBuffer.push_back(BoneData[Index].FinalWeights[2]);
			else
				VertexBuffer.push_back(0);
			if (BoneData[Index].Count > 3)
				VertexBuffer.push_back(BoneData[Index].FinalWeights[3]);
			else
				VertexBuffer.push_back(0);
		}
	}

	// Write it all
	std::ofstream OFS("b:/vscode_projects/glitchheartgame2017/assets/models/hatman.glim", std::ofstream::binary);

	model_header Format = {};
	Format.Format[0] = 'G';
	Format.Format[1] = 'L';
	Format.Format[2] = 'I';
	Format.Format[3] = 'M';

	Format.Version[0] = '1';
	Format.Version[1] = '.';
	Format.Version[2] = '6';

	ModelData.NumMeshes = Entries.size();
	ModelData.MeshChunkSize = Entries.size() * sizeof(mesh_data);
	ModelData.VertexBufferChunkSize = VertexBuffer.size() * sizeof(float);
	ModelData.IndexBufferChunkSize = Indices.size() * sizeof(unsigned int);
	ModelData.NumMaterials = Materials.size();
	ModelData.MaterialChunkSize = Materials.size() * sizeof(material);
	ModelData.BoneChunkSize = Bones.size() * sizeof(bone);
	ModelData.Type = ModelData.NumBones > 0 ? 1 : 0;

	animation_header AnimationHeader;
	AnimationHeader.NumAnimations = SkeletalAnimations.size();
	
	OFS.write(reinterpret_cast<const char*>(&Format), sizeof(model_header));
	OFS.write(reinterpret_cast<const char*>(&ModelData), sizeof(model_data));
	OFS.write(reinterpret_cast<const char*>(&Entries[0]), ModelData.MeshChunkSize);
	OFS.write(reinterpret_cast<const char*>(&VertexBuffer[0]), ModelData.VertexBufferChunkSize);
	OFS.write(reinterpret_cast<const char*>(&Indices[0]), ModelData.IndexBufferChunkSize);
	
	if(ModelData.NumMaterials > 0)
		OFS.write(reinterpret_cast<const char*>(&Materials[0]), ModelData.MaterialChunkSize);
	if(ModelData.NumBones > 0)
		OFS.write(reinterpret_cast<const char*>(&Bones[0]), ModelData.BoneChunkSize);
	if(AnimationHeader.NumAnimations > 0)
		OFS.write(reinterpret_cast<const char*>(&AnimationHeader), sizeof(animation_header));

	for (int AIndex = 0; AIndex < SkeletalAnimations.size(); AIndex++)
	{
		auto Animation = SkeletalAnimations[AIndex];

		animation_channel_header ChannelHeader;
		ChannelHeader.Duration = Animation.Duration;
		ChannelHeader.NumBoneChannels = Animation.NumBoneChannels;
		OFS.write(reinterpret_cast<const char*>(&ChannelHeader), sizeof(animation_channel_header));

		for (int Index = 0; Index < Animation.NumBoneChannels; Index++)
		{
			auto Channel = Animation.BoneChannels[Index];
			bone_animation_header Header;
			Header.BoneIndex = Channel.BoneIndex;
			Header.NumPositionChannels = Channel.PositionKeys.NumKeys;
			Header.NumRotationChannels = Channel.RotationKeys.NumKeys;
			Header.NumScalingChannels = Channel.ScalingKeys.NumKeys;

			OFS.write(reinterpret_cast<const char*>(&Header), sizeof(bone_animation_header));

			OFS.write(reinterpret_cast<const char*>(&Channel.PositionKeys.TimeStamps[0]), sizeof(float) * Header.NumPositionChannels);
			OFS.write(reinterpret_cast<const char*>(&Channel.PositionKeys.Values[0]), sizeof(vec3) * Header.NumPositionChannels);

			OFS.write(reinterpret_cast<const char*>(&Channel.RotationKeys.TimeStamps[0]), sizeof(float) * Header.NumRotationChannels);
			OFS.write(reinterpret_cast<const char*>(&Channel.RotationKeys.Values[0]), sizeof(quat) * Header.NumRotationChannels);

			OFS.write(reinterpret_cast<const char*>(&Channel.ScalingKeys.TimeStamps[0]), sizeof(float) * Header.NumScalingChannels);
			OFS.write(reinterpret_cast<const char*>(&Channel.ScalingKeys.Values[0]), sizeof(vec3) * Header.NumScalingChannels);
		}
	}

	OFS.close();

	return 0;
}

